// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'daily-planner-692af',
    appId: '1:322049244518:web:1dd964d0d2d12f34a11ca7',
    storageBucket: 'daily-planner-692af.appspot.com',
    apiKey: 'AIzaSyCCQzFplVjULCVbzGjJvxC2-nSAbxeb2lQ',
    authDomain: 'daily-planner-692af.firebaseapp.com',
    messagingSenderId: '322049244518',
    measurementId: 'G-CDS31FK5HT',
  },
  production: false
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
