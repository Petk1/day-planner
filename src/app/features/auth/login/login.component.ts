import { Component, inject } from '@angular/core';
import { AutoUnsubscribe } from 'src/app/shared/decorators/auto-unsubscribe';
import { Subscription } from 'rxjs';
import { FormService } from './../form.service';


@AutoUnsubscribe()
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [FormService],
})
export class LoginComponent {
  public formSrv = inject(FormService);
  private signIn!: Subscription;

  onLogin() {
    this.signIn = this.formSrv.signIn().subscribe();
  }
}
