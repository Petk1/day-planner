import { Component, OnInit, inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs';
import { catchError, throwError } from 'rxjs';

import { AutoUnsubscribe } from 'src/app/shared/decorators/auto-unsubscribe';
import { FormService } from '../form.service';
import { NotificationService } from './../../../public/services/notification.service';


@AutoUnsubscribe()
@Component({
  selector: 'app-confirm-change-password',
  templateUrl: './confirm-change-password.component.html',
  styleUrls: ['./confirm-change-password.component.scss'],
  providers: [FormService],
})
export class ConfirmChangePasswordComponent implements OnInit {
  public formSrv = inject(FormService);
  private route = inject(ActivatedRoute);
  private notificationSrv = inject(NotificationService);
  private changePassword!: Subscription;
  protected oobCode!: string;
  protected passwordChanged: boolean = false;
  protected resendEmailOptionVisible: boolean = false;


  ngOnInit(): void {
    this.oobCode = this.route.snapshot.queryParams['oobCode'];
    if(!this.oobCode) this.noOoobCodeNotification();
  }

  noOoobCodeNotification(){
    const message = `The "oobCode" parameter required for password reset could not be read.
    Please try again by clicking the link in the email.
    If this does not work, try resending the email using option "Resend the email" below the form.`;

    this.notificationSrv.createAndOpenNewNotification({
      title: 'Missing parameter',
      message,
      mode: 'error',
      closeAllModalFirst: true,
      height: '425px',
    })
  }

  passwordChangedNotification(){
    const message = 'Your password has been changed successfully. Try logging in using the new password.';

    this.notificationSrv.createAndOpenNewNotification({
      title: 'Password changed',
      message,
      mode: 'information',
      closeAllModalFirst: true
    })
  }

  notTheSamePasswordsNotification(){
    const message = '"Password" and "Confirm Password" must be the same.';

    this.notificationSrv.createAndOpenNewNotification({
      title: 'Incorrect data',
      message,
      mode: 'error',
      closeAllModalFirst: true
    })
  }

  passwordAlreadyChangedNotification(){
    const message = 'Your password has already been changed.';

    this.notificationSrv.createAndOpenNewNotification({
      title: 'Password already changed',
      message,
      mode: 'error',
      closeAllModalFirst: true
    })
  }

  onChangePassword(){
    if(!this.oobCode) {
      this.noOoobCodeNotification();
      return;
    }

    if(this.passwordChanged) {
      this.passwordAlreadyChangedNotification();
      return;
    }

    const password = this.formSrv.password?.value;
    const confirmPassword = this.formSrv.confirmPassword?.value;

    if(password !== confirmPassword){
      this.notTheSamePasswordsNotification();
      return;
    }

    this.changePassword = this.formSrv.changePassword(this.oobCode)
      .pipe(
        catchError(error => {
          this.resendEmailOptionVisible = true;
          return throwError(() => error);
        })
      )
    .subscribe({
      next: () => {
        this.passwordChanged = true;
        this.passwordChangedNotification();
      }
    });
  }
}
