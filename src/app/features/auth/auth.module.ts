import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';

import { AuthRoutingModule } from './auth-routing.module';
import { SharedModule } from '../../shared/shared.module';

import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { ChangePasswordSendMailComponent } from './change-password/change-password-send-mail.component';
import { ConfirmChangePasswordComponent } from './confirm-change-password/confirm-change-password.component';
import { GenericFormComponent } from './generic-form/generic-form.component';


@NgModule({
  declarations: [
    RegistrationComponent,
    LoginComponent,
    ChangePasswordSendMailComponent,
    ConfirmChangePasswordComponent,
    GenericFormComponent,
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    MatDialogModule,
  ]
})
export class AuthModule { }
