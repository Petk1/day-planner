import { Component, inject } from '@angular/core';
import { AutoUnsubscribe } from 'src/app/shared/decorators/auto-unsubscribe';
import { Subscription } from 'rxjs';
import { FormService } from './../form.service';


@AutoUnsubscribe()
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  providers: [FormService],
})
export class RegistrationComponent  {
  public formSrv = inject(FormService);
  private signUp!: Subscription;

  onSignUp() {
    this.signUp = this.formSrv.signUp().subscribe();
  }
}
