import { Component, OnInit, inject } from '@angular/core';

import { Subscription } from 'rxjs';

import { AutoUnsubscribe } from 'src/app/shared/decorators/auto-unsubscribe';
import { FormService } from '../form.service';
import { NotificationService } from 'src/app/public/services/notification.service';


@AutoUnsubscribe()
@Component({
  selector: 'app-change-password-send-mail',
  templateUrl: './change-password-send-mail.component.html',
  providers: [FormService],
})
export class ChangePasswordSendMailComponent implements OnInit {
  public formSrv = inject(FormService);
  private notificationSrv = inject(NotificationService);
  private changePassword!: Subscription;

  ngOnInit(): void {
    this.formSrv.createForm('change-password-send-email');
  }

  onSendEmail(){
    this.changePassword = this.formSrv.changePasswordSendEmail().subscribe({
      next: () => {
        this.notificationSrv.createAndOpenNewNotification({
          title: 'Change password',
          message: 'Email was sent. Check your email.',
          mode: 'information',
          closeAllModalFirst: true,
        });
      }
    });
  }
}
