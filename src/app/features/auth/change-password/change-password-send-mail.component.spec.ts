import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePasswordSendMailComponent } from './change-password-send-mail.component';


describe('ChangePasswordSendMailComponent', () => {
  let component: ChangePasswordSendMailComponent;
  let fixture: ComponentFixture<ChangePasswordSendMailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangePasswordSendMailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChangePasswordSendMailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
