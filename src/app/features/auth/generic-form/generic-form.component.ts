import { Component, inject, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { FormService } from '../form.service';

import { IFormField, TForm } from 'src/app/models/auth-form-field.model';


@Component({
  selector: 'app-generic-form',
  templateUrl: './generic-form.component.html',
  styleUrls: ['./generic-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GenericFormComponent  {
  public readonly formSrv = inject(FormService);
  public formFields: IFormField[] = [];

  @Input() set formType(value: TForm) {
    this.formFields = this.formSrv.createForm(value);
  };

  @Input() title!: string;
  @Input() welcomeMsg?: string;

  @Output() formSubmit: EventEmitter<any> = new EventEmitter();

  onSubmit() {
    this.formSubmit.emit();
  }
}
