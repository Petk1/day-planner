import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PathNames } from 'src/app/app-routing.module';

import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { ChangePasswordSendMailComponent } from './change-password/change-password-send-mail.component';
import { ConfirmChangePasswordComponent } from './confirm-change-password/confirm-change-password.component';


const routes: Routes = [
  {
    path: PathNames.registration,
    component: RegistrationComponent
  },
  {
    path: PathNames.login,
    component: LoginComponent,
  },
  {
    path: PathNames.changePasswordSendMail,
    component: ChangePasswordSendMailComponent,
  },
  {
    path: PathNames.confirmChangePassword,
    component: ConfirmChangePasswordComponent,
  },
  {
    path: '',
    redirectTo: PathNames.login,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
