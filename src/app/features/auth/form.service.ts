import { Injectable, inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { EMPTY, tap } from 'rxjs';

import { AuthService } from 'src/app/core/services/auth.service';
import { IFormField, TForm, TInputType } from 'src/app/models/auth-form-field.model';

import { emailValidator } from 'src/app/shared/form-validators/email.validator';


@Injectable()
export class FormService {
  private fb = inject(FormBuilder);
  private authSrv = inject(AuthService);
  private router = inject(Router);
  public form!: FormGroup;


  createForm(formType: TForm) {
    if(formType === 'signUp'){
      this.form = this.fb.group({
        email: ['', [Validators.required, emailValidator()]],
        password: ['', [Validators.required, Validators.minLength(5)]]
      });

      const formFields = [
        this.getFormField('email', 'email', 'text'),
        this.getFormField('password', 'password', 'password'),
      ]

      return formFields
    }

    if(formType === 'singIn') {
      this.form = this.fb.group({
        email: ['', [Validators.required, emailValidator()]],
        password: ['', [Validators.required]]
      });

      const formFields = [
        this.getFormField('email', 'email', 'text'),
        this.getFormField('password', 'password', 'password'),
      ]

      return formFields
    }

    if(formType === 'change-password-send-email'){
      this.form = this.fb.group({
        email: ['', [Validators.required, emailValidator()]],
      });

      const formFields = [
        this.getFormField('email', 'email', 'text'),
      ]

      return formFields
    }

    if(formType === 'change-password'){
      this.form = this.fb.group({
        password: ['', [Validators.required, Validators.minLength(5)]],
        confirmPassword: ['', [Validators.required, Validators.minLength(5)]]
      });

      const formFields = [
        this.getFormField('password', 'password', 'password'),
        this.getFormField('confirmPassword', 'confirm password', 'password'),
      ]

      return formFields
    }

    return [];
  }

  getFormField(nameFormControl: string, label: string, typeInput: TInputType): IFormField {
    return { nameFormControl, label, typeInput }
  }

  signIn(){
    if(this.form.invalid){
      return this.handleInvalidForm();
    }

    const { email, password } = this.allFormValues;
    return this.authSrv.signIn(email, password).pipe(tap(() => this.navigateToCalendar()));
  }

  signUp(){
    if(this.form.invalid){
      return this.handleInvalidForm();
    }

    const { email, password } = this.allFormValues;
    return this.authSrv.signUp(email, password).pipe(tap(() => this.navigateToCalendar()));
  }

  changePasswordSendEmail(){
    if(this.form.invalid){
      return this.handleInvalidForm();
    }

    const email = this.email?.value;
    return this.authSrv.changePasswordSendEmail(email);
  }

  changePassword(oobCode: string){
    if(this.form.invalid){
      return this.handleInvalidForm();
    }

    const password = this.password?.value;
    return this.authSrv.changePassword(password, oobCode).pipe(tap(() => this.navigateToLogin()));
  }

  handleInvalidForm(){
    this.form.markAllAsTouched();
    return EMPTY;
  }

  private navigateToCalendar() {
    return this.router.navigate(['/calendar']);
  }

  private navigateToLogin() {
    return this.router.navigate(['/login']);
  }

  get email() {
    return this.form.get('email');
  }

  get password() {
    return this.form.get('password');
  }

  get confirmPassword() {
    return this.form.get('confirmPassword');
  }

  get emailErrors() {
    return this.email?.errors;
  }

  get passwordErrors() {
    return this.password?.errors;
  }

  get confirmPasswordErrors() {
    return this.confirmPassword?.errors;
  }

  get allFormValues() {
    return {
      email: this.email?.value,
      password: this.password?.value,
      confirmPassword: this.confirmPassword?.value,
    }
  }

}
