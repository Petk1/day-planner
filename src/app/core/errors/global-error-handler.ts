import { Injectable, ErrorHandler, inject, NgZone } from "@angular/core";

import { NotificationService } from "src/app/public/services/notification.service";
import { getErrorMessageByCode } from "src/app/shared/units/error-codes";


@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  private zone = inject(NgZone);
  private notificationSrv = inject(NotificationService);
  private isOnline: boolean = true;

  handleError(error: any): void {
    console.error('Error occured: ', error);

    // connectionStatus - information from effect checkNetworkConnection$
    if(error && typeof error === 'object' && 'connectionStatus' in error){
      this.isOnline = error.connectionStatus
    }

    if(!this.isOnline){
      this.zone.run(() => {
        this.notificationSrv.createAndOpenNewNotification({
          title: 'Connection lost',
          message: 'You are not connected to internet.',
          mode: 'lostConnection',
          closeAllModalFirst: true,
        });
      })

      return;
    }

    const errorCode = error?.rejection?.code || error?.code;
    if(errorCode) {
      const errorInfo = getErrorMessageByCode(errorCode);
      this.notificationSrv.createAndOpenNewNotification({
        ...errorInfo,
        mode: 'error',
        closeAllModalFirst: true
      });

      return;
    }
  }

  setIsOnline(value: boolean){
    this.isOnline = value;
  }
}
