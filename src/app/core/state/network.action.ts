import { createAction, props } from "@ngrx/store";


const prefixName = '[network]';

export const CHECK_CONNECTION_STATUS_ACTION = `${prefixName} check connection status`;
export const UPDATE_CONNECTION_STATUS_ACTION = `${prefixName} update connection status`;

export const checkConnectionStatus = createAction(CHECK_CONNECTION_STATUS_ACTION);
export const updateConnectionStatus = createAction(UPDATE_CONNECTION_STATUS_ACTION, props<{ isConnected: boolean }>());
