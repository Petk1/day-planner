import { INetworkConnectionInformation } from './../../models/network-connection-information.model';
import { throwError } from 'rxjs';
import { Injectable, inject, ErrorHandler } from '@angular/core';

import { map, switchMap, merge, fromEvent } from 'rxjs';

import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { AppState } from 'src/app/state/app.state';
import { checkConnectionStatus, updateConnectionStatus } from './network.action';


@Injectable()
export class NetworkEffects {
  private actions$ = inject(Actions);
  private store = inject(Store<AppState>);
  private errorHandler = inject(ErrorHandler);

  checkNetworkConnection$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(checkConnectionStatus),
      switchMap(() => {
        return merge(
          fromEvent(window, "online").pipe(map(() => true)),
          fromEvent(window, "offline").pipe(map(() => false)),
        );
      }),
      map((isConnected: boolean) => {
        const connectionInformation: INetworkConnectionInformation = { connectionStatus: isConnected }
        this.errorHandler.handleError(connectionInformation);
        return updateConnectionStatus({ isConnected });
      })
    )
  })

}
