import { Action, createReducer, on } from "@ngrx/store";

import { NetworkState, initialState } from "./network.state";
import { updateConnectionStatus } from "./network.action";


const _networkReducer = createReducer(initialState,
  on(updateConnectionStatus, (state, action) => {
    return {
      ...state,
      isConnected: action.isConnected
    }
  }),
);

export function networkReducer(state: NetworkState | undefined, action: Action) {
  return _networkReducer(state, action);
}
