import { createFeatureSelector, createSelector } from "@ngrx/store";
import { NetworkState } from "./network.state";


export const NETWORK_STATE_NAME = 'network';

const getNetworkState = createFeatureSelector<NetworkState>(NETWORK_STATE_NAME);

export const isConnected = createSelector(getNetworkState, state => {
  return state.isConnected;
})
