export interface NetworkState {
  isConnected: boolean;
}

export const initialState: NetworkState = {
  isConnected: true,
}
