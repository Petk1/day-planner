import { ErrorHandler, Injector, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GlobalErrorHandler } from './errors/global-error-handler';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { NetworkEffects } from './state/network.effects';
import { networkReducer } from './state/network.reducer';
import { NETWORK_STATE_NAME } from './state/network.selector';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(NETWORK_STATE_NAME, networkReducer),
    EffectsModule.forFeature([ NetworkEffects ]),
  ],
  providers: [
    {
      provide: ErrorHandler,
      useClass: GlobalErrorHandler,
    }
  ]
})
export class CoreModule {
  static injector: Injector;

  constructor(injector: Injector) {
    CoreModule.injector = injector;
  }
}
