import { Injectable } from "@angular/core";


@Injectable({
  providedIn: 'root'
})
export class LocalStorageService { // service to handle all data stored in localStorage

  // user trying to change password can send email once every 10 minutes
  howTimeLeftToAttemptChangePassword(email: string){
    const lastSent = this.getItem(`lastSent:${email}`);

    if(!lastSent) return null;

    const now = new Date();
    const lastSentDate = new Date(lastSent);
    const diff = now.getTime() - lastSentDate.getTime();
    const minutesPassed = Math.floor(diff / 1000 / 60);

    return minutesPassed > 10 ? null : 10 - minutesPassed;
  }

  getItem(key: string){
    return localStorage.getItem(key);
  }

  setItem(key: string, value: string){
    localStorage.setItem(key, value);
  }
}
