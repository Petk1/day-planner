import { Injectable, inject } from '@angular/core';
import { UserInfo } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/compat/auth';

import { from, of, pipe, throwError } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';

import { LocalStorageService } from './local-storage.service';
import { NotificationService } from './../../public/services/notification.service';

import { WaitingForResult } from 'src/app/shared/decorators/waiting-for-result';
import { MSG_USER_NOT_FOUND } from 'src/assets/messages/messages';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly auth = inject(AngularFireAuth);
  private readonly localStorageSrv = inject(LocalStorageService);
  private readonly notificationSrv = inject(NotificationService);


  @WaitingForResult()
  signIn(email: string, password: string) {
    return from(this.auth.signInWithEmailAndPassword(email, password));
  }

  @WaitingForResult()
  signUp(email: string, password: string){
    return from(this.auth.createUserWithEmailAndPassword(email, password));
  }

  @WaitingForResult()
  changePasswordSendEmail(email: string){
    const timeLeft = this.localStorageSrv.howTimeLeftToAttemptChangePassword(email);

    if(typeof timeLeft === 'number'){
      const message = `You have to wait ${timeLeft} minutes before sending next email.`;
      this.notificationSrv.createAndOpenNewNotification({
        title: 'Please wait',
        message,
        mode: 'error',
        closeAllModalFirst: true,
      });

      return throwError(() => message);
    }

    return this.checkAccountEmailExist(email)
      .pipe(
        switchMap(isEmaiLExist => {
          if(isEmaiLExist){
            this.localStorageSrv.setItem(`lastSent:${email}`, new Date().toString());
            return from(this.auth.sendPasswordResetEmail(email))
          }

          const message = 'There is no account associated with the provided email address.'
          this.notificationSrv.createAndOpenNewNotification({
            title: 'The account does not exist.',
            message,
            mode: 'error',
            closeAllModalFirst: true,
          });

          return throwError(() => message);
        })
      )
  }

  @WaitingForResult()
  changePassword(password: string, oobCode: string){
    return from(this.auth.confirmPasswordReset(oobCode, password));
  }

  getUserInfo(){
    return from(this.auth.currentUser).pipe(take(1));
  }

  @WaitingForResult()
  logout(){
    return from(this.auth.signOut());
  }

  checkAccountEmailExist(email: string){
    return from(this.auth.fetchSignInMethodsForEmail(email))
      .pipe(
        map(signInMethods => {
          if(!signInMethods || !signInMethods.length) return false;
          return true;
        })
      )
  }

  checkUid() {
    return pipe(
      map((user: UserInfo | null) => {
        const uid = user?.uid;

        if(!uid){
          throw new Error(MSG_USER_NOT_FOUND);
        }

        return uid;
      })
    )
  }

}
