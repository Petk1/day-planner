import { Injectable, inject } from '@angular/core';

import { Store } from '@ngrx/store';
import { AppState } from 'src/app/state/app.state';
import { isConnected } from '../state/network.selector';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class NetworkConnectionService {
  private store = inject(Store<AppState>);


  public checkConnection<T>(action?: T): Observable<T | boolean> {
    return this.store.select(isConnected)
      .pipe(
        map((isConnected) => {
          if (!isConnected) {
            throw new Error('Lost connection');
          }

          return action || isConnected;
        })
      );
  }

}
