export function getErrorMessageByCode(code?: string) {
  const errorInfo = {
    title: 'An error has occured',
    message: 'Something went wrong.',
    height: '275px',
  }

  if(!code) return errorInfo;

  switch(code) {
    case 'auth/user-not-found':
      errorInfo.message = 'User not found.';
      break;
    case 'auth/wrong-password':
      errorInfo.message = 'Wrong password.';
      break;
    case 'auth/too-many-requests':
      errorInfo.message = 'Too many attempts.';
      break;
    case 'auth/email-already-exists':
      errorInfo.message = 'Email already exists.';
      break;
    case 'auth/email-already-in-use':
      errorInfo.message = 'This email is aready in use. Enter another and try again.';
      break;
    case 'auth/network-request-failed':
      errorInfo.message = 'Interrupted connection or unreachable host. Check your connection.';
      break;
    case 'auth/invalid-action-code':
      errorInfo.message = `The action code is invalid.
      This can happen if the code is malformed, expired, or has already been used.
      If you still want to change your password, try resending the email using option "Resend the email" below the form.`;
      errorInfo.height = '440px';
      break;
  }

  return errorInfo;
}
