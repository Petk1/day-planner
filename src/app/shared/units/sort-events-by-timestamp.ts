import { IEvent } from "src/app/models/event.model";

export function sortEventsByTimestamp(events: IEvent[]){
  events.sort((singleEventA, singleEventB) => singleEventA.timestamp - singleEventB.timestamp);
}
