export class HandleDate {

  static getYearMonthDay(date: Date){
    const timezoneOffset = date.getTimezoneOffset() * 60000;
    const yearMonthDay = new Date(date.getTime() - timezoneOffset).toISOString().substring(0,10);
    return yearMonthDay;
  }

  static getDateMinusTimezonOffset(date: Date){
    const timezoneOffset = date.getTimezoneOffset() * 60000;
    return new Date(date.getTime() + timezoneOffset);
  }
}
