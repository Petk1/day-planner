import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { INotificationInitData, TNotificationMode } from 'src/app/models/notification-config.model';
import { checkObjectProp } from 'src/app/shared/units/check-object-prop';


@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NotificationComponent {
  title: string = '';
  message: string = '';
  mode: TNotificationMode = 'information';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: INotificationInitData,
    private dialogRef: MatDialogRef<NotificationComponent>,
  ) {
    this.checkInitData(data);
  }

  private checkInitData(data: INotificationInitData) {
    Object.keys(data).forEach(prop => {

      if(checkObjectProp(data, prop, 'string')){
        const propName = prop as keyof INotificationInitData;

        if(propName === 'mode'){
          this.mode = data[propName] || 'information';
          return;
        }

        this[propName] = data[propName] || '';
      }
    })
  }

  onClose(result: boolean){
    this.dialogRef.close(result);
  }
}
