import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';


@Component({
  selector: 'app-form-errors',
  templateUrl: './form-errors.component.html',
  styleUrls: ['./form-errors.component.scss'],
})
export class FormErrorsComponent {
  @Input() control!: FormControl;

  readonly requiredField: string = 'This field is required';
  readonly invalidEmailMsg: string = 'Enter a valid email';
  readonly passwordMinLengthMsg: string = 'Minimum password length is 5 characters';
}
