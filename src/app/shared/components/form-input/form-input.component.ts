import { Component, Input, OnInit, inject } from '@angular/core';
import { FormControl, FormGroupDirective } from '@angular/forms';
import { IFormField, TInputType } from 'src/app/models/auth-form-field.model';


@Component({
  selector: 'app-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.scss'],
})
export class FormInputComponent implements OnInit {
  @Input() set inputData(value: IFormField) {
    this.label = value.label;
    this.typeInput = value.typeInput;
    this.nameFormControl = value.nameFormControl;
  };

  label!: string;
  typeInput!: TInputType;
  nameFormControl!: string;
  formControl!: FormControl;
  private rootFormGroup = inject(FormGroupDirective);

  ngOnInit(): void {
    this.formControl = this.rootFormGroup.control.get(this.nameFormControl) as FormControl;
  }
}
