import { Component, ChangeDetectionStrategy, Input, inject } from '@angular/core';

import { Store } from '@ngrx/store';
import { AppState } from 'src/app/state/app.state';
import { logoutAction } from './../../../state/app.action';

import { PathNames as AppPathNames } from './../../../app-routing.module';

@Component({
  selector: 'app-app-bar',
  templateUrl: './app-bar.component.html',
  styleUrls: ['./app-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppBarComponent {
  @Input() urlPath: string = '';

  store = inject(Store<AppState>);
  pathNames = AppPathNames;

  onLogout() {
    this.store.dispatch(logoutAction());
  }
}
