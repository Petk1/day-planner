import { CoreModule } from 'src/app/core/core.module';
import { NetworkConnectionService } from 'src/app/core/services/network-connection.service';

import { catchError, switchMap } from 'rxjs/operators';
import { throwError, tap } from 'rxjs';


export function FirestoreErrorHandler(target: any, key: string, descriptor: PropertyDescriptor) {
  const originalMethod = descriptor.value;

  descriptor.value = function(...args: any[]) {
    const networkConnectionSrv = CoreModule.injector.get<NetworkConnectionService>(NetworkConnectionService);

    return networkConnectionSrv.checkConnection().pipe(
      switchMap(() => originalMethod.apply(this, args)),
      catchError(error => throwError(() => error))
    )
  };

  return descriptor;
}
