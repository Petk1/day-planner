import { SharedModule } from "../shared.module";
import { Observable, catchError, tap, throwError } from "rxjs";
import { NotificationService } from "src/app/public/services/notification.service";
import { INotificationConfig } from "src/app/models/notification-config.model";


export function WaitingForResult(title: string = 'Action in progress', message: string = 'Please wait...'): MethodDecorator {
  return function(target: any, key: string | symbol, descriptor: PropertyDescriptor) {
    const originalMethod = descriptor.value;

    descriptor.value = function(...args: any[]) {
      const result = originalMethod.apply(this, args);

      if(result instanceof Observable){
        const notificationSrv = SharedModule.injector.get<NotificationService>(NotificationService);
        const notificationConfig: INotificationConfig = { title, message, mode: 'waiting' };
        notificationSrv.createAndOpenNewNotification(notificationConfig);

        return result.pipe(
          catchError((error: any) => {
            notificationSrv.closeAllNotifications();
            return throwError(() => error);
          }),
          tap(() => notificationSrv.closeAllNotifications()
        ));
      }

      return result;
    }

    return descriptor;
  }
}
