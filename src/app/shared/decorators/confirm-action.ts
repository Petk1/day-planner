import { SharedModule } from "../shared.module";
import { take } from "rxjs";
import { NotificationService } from "src/app/public/services/notification.service";
import { INotificationConfig } from "src/app/models/notification-config.model";


export function ConfirmAction(title: string, message: string): MethodDecorator {
  return function(target: any, key: string | symbol, descriptor: PropertyDescriptor) {
    const originalMethod = descriptor.value;

    descriptor.value = function(...args: any[]) {
      const notificationSrv = SharedModule.injector.get<NotificationService>(NotificationService);
      const notificationConfig: INotificationConfig = { title, message, mode: 'confirm' };

      notificationSrv.createAndOpenNewNotification(notificationConfig)
        .afterClosed()
        .pipe(take(1))
        .subscribe(decision => {
          if(decision){
            return originalMethod.apply(this, args);;
          }

          return null;
        });
    };

    return descriptor;
  };
}
