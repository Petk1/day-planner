import { Subscription } from 'rxjs';


export function AutoUnsubscribe() {
  return function<T extends { new(...args: any[]): {} }>(constructor: T) {
    const original = constructor.prototype.ngOnDestroy;

    constructor.prototype.ngOnDestroy = function() {
      for(const prop in this){
        const property = this[prop];

        if(property instanceof Subscription && property){
          property.unsubscribe();
        } else if(property instanceof Array && property.length && property[0] instanceof Subscription){
          (<Subscription[]>property).forEach(subscription => subscription.unsubscribe());
        }
      }

      if (original && typeof original === 'function') {
        original.apply(this, arguments);
      }
    };
  };
}
