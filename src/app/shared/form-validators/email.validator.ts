import { AbstractControl, ValidatorFn, ValidationErrors } from "@angular/forms";

export const emailValidator = (): ValidatorFn => {
    return (control: AbstractControl): ValidationErrors | null => {
        if(!control.value) return null;
        const testResult = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g.test(control.value);
        return !testResult ? { emailRegex: true } : null;
    }
}
