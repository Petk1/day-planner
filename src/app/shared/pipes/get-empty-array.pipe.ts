import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'getEmptyArray'
})
export class GetEmptyArrayPipe implements PipeTransform {

  transform(length: number) {
    return new Array(length);
  }
}
