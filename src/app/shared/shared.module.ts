import { Injector, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';

import { MatIconModule } from '@angular/material/icon';

import { NotificationService } from 'src/app/public/services/notification.service';

import { AppBarComponent } from './components/app-bar/app-bar.component';
import { FormInputComponent } from './components/form-input/form-input.component';
import { FormErrorsComponent } from './components/form-errors/form-errors.component';
import { NotificationComponent } from './modals/notification/notification.component';
import { BackgroundAnimationComponent } from './components/background-animation/background-animation.component';

import { GetEmptyArrayPipe } from './pipes/get-empty-array.pipe';


@NgModule({
  declarations: [
    AppBarComponent,
    BackgroundAnimationComponent,
    FormInputComponent,
    FormErrorsComponent,
    NotificationComponent,
    GetEmptyArrayPipe,
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatIconModule,
  ],
  providers: [
    NotificationService,
  ],
  exports: [
    AppBarComponent,
    BackgroundAnimationComponent,
    FormInputComponent,
    NotificationComponent,
    GetEmptyArrayPipe,
  ]
})
export class SharedModule {
  static injector: Injector;

  constructor(injector: Injector) {
    SharedModule.injector = injector;
  }
}
