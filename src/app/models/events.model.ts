import { IEvent } from 'src/app/models/event.model';

export interface IEvents{
  [year: string]: {
    [month: string]: {
      [day: string]: IEvent[]
    }
  }
}
