export interface INetworkConnectionInformation {
  message?: string;
  connectionStatus: boolean;
}
