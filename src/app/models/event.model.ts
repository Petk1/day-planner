export interface IEvent {
  id: string;
  name: string;
  content: string;
  done: boolean;
  timestamp: number;
}
