export interface IFormField {
  nameFormControl: string;
  label: string;
  typeInput: TInputType;
}

export type TInputType = 'text' | 'password';

export type TForm = 'singIn' | 'signUp' | 'change-password-send-email' | 'change-password';
