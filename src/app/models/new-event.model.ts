export interface INewEvent {
  name: string;
  content: string;
  done: boolean;
  date: Date;
}
