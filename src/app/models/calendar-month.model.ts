import { IDay } from "./calendar-day.model";

export interface IMonth {
  firstDayNumber: number,
  numberDays: number,
  days: IDay[]
}
