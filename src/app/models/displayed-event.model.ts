import { IEvent } from "./event.model";

export interface IDisplayedEvent extends IEvent {
  selected: boolean;
}
