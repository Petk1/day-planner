export interface IDateFromDb {
  nanoseconds: number;
  seconds: number;
}
