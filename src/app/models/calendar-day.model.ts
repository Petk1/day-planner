import { IEvent } from "./event.model";

export interface IDay {
  dayNumber: number,
  isActive: boolean,
  events?: IEvent[]
}
