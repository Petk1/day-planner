import { IDateFromDb } from './date-from-db';

export interface IEventFromDb {
  id: string;
  name: string;
  content: string;
  done: boolean;
  date: IDateFromDb;
}
