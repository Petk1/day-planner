import { IEvent } from './event.model';
import { IDate } from "./date.model";
import { INewEvent } from "./new-event.model";

export interface IActionPropsNewEvent {
  addedEvent: IEvent;
  date: IDate;
  timestamp?: number;
}
