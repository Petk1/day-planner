export interface INotificationConfig extends INotificationInitData {
  width?: string;
  height?: string;
  closeAllModalFirst?: boolean;
}

export interface INotificationInitData {
  title: string;
  message: string;
  mode?: TNotificationMode;
}

export type TNotificationMode = 'information' | 'error' | 'confirm' | 'waiting' | 'lostConnection';
