import { IDate } from 'src/app/models/date.model';

export interface IUpdateEvent {
  id: string;
  name: string;
  content: string;
  done: boolean;
  date?: Date;
  dateObject?: IDate;
}
