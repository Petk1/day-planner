import { inject, Injectable } from "@angular/core";
import { Router } from "@angular/router";

import { Actions, createEffect, ofType } from "@ngrx/effects";
import { logoutAction, logoutActionSuccess } from './app.action';

import { map, switchMap } from 'rxjs';

import { AuthService } from '../core/services/auth.service';


@Injectable()
export class AppEffects {
  actions$ = inject(Actions);
  authSrv = inject(AuthService);
  router = inject(Router);

  logout$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(logoutAction),
      switchMap(() => {
        return this.authSrv.logout().pipe(
          map(() => {
            this.router.navigate(['/login']);
            return logoutActionSuccess()
          })
        )
      })
    )
  })

}
