import { EVENTS_STATE_NAME } from '../public/components/calendar/state/events.selector';
import { NETWORK_STATE_NAME } from '../core/state/network.selector';

import { EventsState } from '../public/components/calendar/state/events.state';
import { NetworkState } from '../core/state/network.state';

import { eventsReducer } from '../public/components/calendar/state/events.reducer';
import { networkReducer } from '../core/state/network.reducer';


export interface AppState {
  [EVENTS_STATE_NAME]: EventsState,
  [NETWORK_STATE_NAME]: NetworkState,
}

export const appReducer = {
  [EVENTS_STATE_NAME]: eventsReducer,
  [NETWORK_STATE_NAME]: networkReducer,
}
