import { Action, ActionReducer, MetaReducer } from '@ngrx/store';
import { LOGOUT_ACTION_SUCCESS } from './app.action';


const clearStateMetaReducer = <State extends {}>(reducer: ActionReducer<State>) => {
  return (state: State, action: Action) => {
    if (action.type === LOGOUT_ACTION_SUCCESS) {
      state = {} as State;
    }

    return reducer(state, action);
 };
}


export const metaReducers: MetaReducer[] = [ clearStateMetaReducer ];
