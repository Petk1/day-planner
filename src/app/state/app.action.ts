import { createAction } from "@ngrx/store";


const prefixName = '[app]';

export const LOGOUT_ACTION = `${prefixName} logout`;
export const LOGOUT_ACTION_SUCCESS = `${prefixName} logout success`;

export const logoutAction = createAction(LOGOUT_ACTION);
export const logoutActionSuccess = createAction(LOGOUT_ACTION_SUCCESS);
