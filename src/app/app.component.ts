import { AppState } from 'src/app/state/app.state';
import { Component, OnInit, inject } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

import { AutoUnsubscribe } from './shared/decorators/auto-unsubscribe';

import { Store } from '@ngrx/store';

import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { checkConnectionStatus } from './core/state/network.action';


@AutoUnsubscribe()
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  private router = inject(Router);
  private store = inject(Store<AppState>);
  subscriptions: Subscription[] = [];
  currentPath!: string;


  ngOnInit(): void {
    this.readCurrentPath();
    this.verifyOnlineStatus();
  }

  readCurrentPath(){
    const readCurrentPathSub = this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map(event => event as NavigationEnd),
      map(event => event?.urlAfterRedirects)
    ).subscribe((path: string) => {
      const indexQuestionMark = path.indexOf('?');
      if(indexQuestionMark !== -1) path = path.substring(0, indexQuestionMark);
      this.currentPath = path.replace('/', '');
    })

    this.subscriptions.push(readCurrentPathSub);
  }

  verifyOnlineStatus(){
    this.store.dispatch(checkConnectionStatus());
  }

}
