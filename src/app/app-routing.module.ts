import { CoreModule } from './core/core.module';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const PathNames = { // using in app-bar.component.ts
  auth: '',
  calendar: 'calendar',
  login: 'login',
  registration: 'registration',
  changePasswordSendMail: 'change-password-send-mail',
  confirmChangePassword: 'confirm-change-password',
  wildcard: '**'
}

const routes: Routes = [
  {
    path: PathNames.auth,
    loadChildren: () => import('./features/auth/auth.module').then(m => m.AuthModule),
  },
  {
    path: PathNames.calendar,
    loadChildren: () => import('./public/public.module').then(m => m.PublicModule),
  },
  {
    path: PathNames.wildcard,
    redirectTo: PathNames.login,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
