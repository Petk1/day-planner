import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { MatDialogModule } from '@angular/material/dialog';
import { SharedModule } from '../shared/shared.module';
import { PublicRoutingModule } from './public-routing.module';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { EventsEffects } from './components/calendar/state/events.effects';
import { eventsReducer } from './components/calendar/state/events.reducer';
import { EVENTS_STATE_NAME } from './components/calendar/state/events.selector';

import { EventsService } from 'src/app/public/services/events.service';

import { DayComponent } from './components/calendar/day/day.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { MainPageComponent } from './components/main-page/main-page.component';
import { EventsComponent } from './components/calendar/events/events.component';
import { DayNamesComponent } from './components/calendar/day-names/day-names.component';
import { HandleEventComponent } from './components/modals/handle-event/handle-event.component';
import { EventDetailsComponent } from './components/modals/event-details/event-details.component';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [
    MainPageComponent,
    CalendarComponent,
    DayComponent,
    DayNamesComponent,
    EventsComponent,
    EventDetailsComponent,
    HandleEventComponent,
  ],
  imports: [
    CommonModule,
    PublicRoutingModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatIconModule,
    DragDropModule,
    SharedModule,
    StoreModule.forFeature(EVENTS_STATE_NAME, eventsReducer, ),
    EffectsModule.forFeature([ EventsEffects ]),
  ],
  providers: [
    EventsService,
  ]
})
export class PublicModule { }
