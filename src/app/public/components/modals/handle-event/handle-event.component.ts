import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { AutoUnsubscribe } from 'src/app/shared/decorators/auto-unsubscribe';

import { Subscription } from 'rxjs';

import { UPDATE_EVENT_SUCCESS_ACTION, ADD_EVENT_SUCCESS_ACTION } from './../../calendar/state/events.action';

import { IDisplayedEvent } from '../../../../models/displayed-event.model';
import { HandleEventService } from 'src/app/public/services/handle-event.service';


interface InitData {
  dateName: string;
  date: Date;
  currentEventData?: IDisplayedEvent;
  editOnlyTime?: boolean;
}

export interface Event {
  name: string;
  content?: string;
  date?: Date;
}

type SubmitBtnName = 'Add Event' | 'Update Event'


@AutoUnsubscribe()
@Component({
  selector: 'app-handle-event',
  templateUrl: './handle-event.component.html',
  styleUrls: ['./handle-event.component.scss'],
  providers: [HandleEventService],
})
export class HandleEventComponent {
  handleEventSuccesSub!: Subscription
  submitBtnName: SubmitBtnName = 'Add Event';
  editOnlyTime: boolean = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: InitData,
    public handleEventSrv: HandleEventService,
    private dialogRef: MatDialogRef<HandleEventComponent>,
  ) {
    this.setVariables();
  }

  setVariables(){
    const { currentEventData, date, editOnlyTime } = this.data;

    if(currentEventData){
      const { id, done } = currentEventData;

      this.submitBtnName = 'Update Event';
      this.handleEventSrv.currentEventId = id;
      this.handleEventSrv.currentEventStatus = done || false;

      if(editOnlyTime){
        this.editOnlyTime = editOnlyTime;
      }
    }

    this.handleEventSrv.setForm(currentEventData);
    this.handleEventSrv.date = date;
  }

  onHandleEvent() {
    if(this.handleEventSrv.currentEventId){
      this.updateEvent();
      return;
    }

    this.addEvent();
  }

  updateEvent(){
    this.handleEventSrv.updateEvent(this.editOnlyTime);
    this.closeModalAfterSuccess(UPDATE_EVENT_SUCCESS_ACTION);
  }

  addEvent(){
    this.handleEventSrv.addEvent();
    this.closeModalAfterSuccess(ADD_EVENT_SUCCESS_ACTION);
  }

  closeModalAfterSuccess(actionName: string){
    this.handleEventSuccesSub = this.handleEventSrv.isActionSuccess(actionName)
    .subscribe(() => {
      this.dialogRef.close(true);
    });
  }

}
