import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, Inject } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

import { Subscription, EMPTY } from 'rxjs';
import { switchMap, take } from 'rxjs/operators';

import { AutoUnsubscribe } from 'src/app/shared/decorators/auto-unsubscribe';

import { EventDetailsService } from 'src/app/public/services/event-details.service';
import { ConfirmAction } from 'src/app/shared/decorators/confirm-action';

import { HandleEventComponent } from '../handle-event/handle-event.component';

import { IEvent } from 'src/app/models/event.model';
import { IDisplayedEvent } from 'src/app/models/displayed-event.model';
import { sortEventsByTimestamp } from 'src/app/shared/units/sort-events-by-timestamp';


interface InitData {
  events: IEvent[],
  date: Date
}

interface IEditTime {
  previousIndex: number;
}


@AutoUnsubscribe()
@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    EventDetailsService,
  ]
})
export class EventDetailsComponent {
  modalDialog: MatDialogRef<HandleEventComponent, any>[] | undefined;
  dialogConfig = new MatDialogConfig();
  date!: Date;
  dateName: string = '';
  selectedEventIndex!: number | null;
  subscriptions: Subscription[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: InitData,
    public eventDetailsSrv: EventDetailsService,
    private matDialog: MatDialog,
    private cdr: ChangeDetectorRef,
  ) {
    const { date, events } = data;
    if(events){
      this.eventDetailsSrv.setEvents(events, true);
    }
    this.date = date;
    this.eventDetailsSrv.setDate(date);
    this.setDateName()
  }

  setDateName(){
    const monthLongName = this.date.toLocaleString('en', { month: 'long' });
    const dayName = this.date.toLocaleString('en', { weekday: 'long' });
    const { day, year } = this.eventDetailsSrv.objectDate;

    this.dateName = `${day} ${monthLongName} ${year} (${dayName})`;
  }

  openModalViewHandleEvent(singleEvent?: IDisplayedEvent, editTimeObj?: IEditTime, dialogConfig?: MatDialogConfig<any>) {
    this.dialogConfig.id = "handle-event-modal-component";
    this.dialogConfig.height = dialogConfig?.height || "620px";
    this.dialogConfig.width = dialogConfig?.width || "650px";
    this.dialogConfig.data = {
      dateName: this.dateName,
      date: this.date,
      currentEventData: singleEvent,
      editOnlyTime: !!editTimeObj,
    };

    const newModalDialog = this.matDialog.open(HandleEventComponent, this.dialogConfig);

    const modalDialogSub = newModalDialog.afterClosed()
      .pipe(
        switchMap(result => {
          if(!result) {

            if(editTimeObj){
              sortEventsByTimestamp(this.eventDetailsSrv.events);
              this.selectEvent(editTimeObj?.previousIndex);
              this.cdr.markForCheck();
            }

            return EMPTY;
          }

          return this.eventDetailsSrv.getEventsByDate()
        }),
        take(1),
      )
      .subscribe(events => {
        this.refreshEvents(events, false);

        if(editTimeObj){
          this.unselectEvent();
        }
      })

    this.subscriptions.push(modalDialogSub);
  }

  selectEvent(index: number){
    if(this.selectedEventIndex === index) return;

    this.unselectEvent();
    this.selectedEventIndex = index;
    this.eventDetailsSrv.events[index].selected = true;
  }

  unselectEvent(){
    if(typeof this.selectedEventIndex === 'number'){
      this.eventDetailsSrv?.events?.forEach(singleEvent => {
        singleEvent.selected = false;
      })
    }
  }

  onAddEvent(){
    this.openModalViewHandleEvent();
  }

  onSwitchDoneNotDoneEvent(event: Event, singleEvent: IDisplayedEvent) {
    event.stopPropagation();

    const switchDoneNotDoneEventSuccessSub = this.eventDetailsSrv.doneNotDoneEventSwitch(singleEvent, this.date)
      .subscribe(events => {
        this.refreshEvents(events, false);
      })

    this.subscriptions.push(switchDoneNotDoneEventSuccessSub);
  }

  onEditEvent(singleEvent: IDisplayedEvent){
    this.openModalViewHandleEvent(singleEvent);
  }

  onEditOnlyTime(singleEvent: IDisplayedEvent, editTimeObj: IEditTime){
    this.openModalViewHandleEvent(singleEvent, editTimeObj, { height: '270px' });
  }

  @ConfirmAction('Confirm', 'Are you sure you want to delete this event?')
  onDeleteEvent(eventId: string) {
    const deleteEventSuccessSub = this.eventDetailsSrv.deleteEvent(eventId)
      .subscribe(events => {
        this.refreshEvents(events, true);
      })

    this.subscriptions.push(deleteEventSuccessSub);
  }

  refreshEvents(events: IEvent[] | null, setAllNotSelected: boolean) {
    if(!events) return;
    if(setAllNotSelected){
      this.selectedEventIndex = null;
    }

    this.eventDetailsSrv.setEvents(events, setAllNotSelected, this.selectedEventIndex);

    if(this.subscriptions?.length){
      this.subscriptions.forEach(subscription => subscription.unsubscribe());
    }
    this.cdr.markForCheck();
  }

  onDrop({ previousIndex, currentIndex }: CdkDragDrop<IDisplayedEvent[]>) {
    if(previousIndex === currentIndex) return;

    const events = this.eventDetailsSrv.events;
    moveItemInArray(events, previousIndex, currentIndex);
    this.selectEvent(currentIndex);
    this.onEditOnlyTime(events[currentIndex], { previousIndex });
  }

}
