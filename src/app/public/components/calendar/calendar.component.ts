import { Component, OnInit, AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild, ElementRef, HostListener, inject } from '@angular/core';

import { AutoUnsubscribe } from 'src/app/shared/decorators/auto-unsubscribe';

import { Subscription } from 'rxjs';

import { CalendarService } from '../../services/calendar.services';


@AutoUnsubscribe()
@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ CalendarService ],
})
export class CalendarComponent implements OnInit, AfterViewInit {
  calendarSrv = inject(CalendarService);
  cdr = inject(ChangeDetectorRef);
  eventSubscriptions!: Subscription;
  readonly singleEventHeight: number = 26; // height in px
  readonly bottomDayMargin: number = 22 // in px;
  maxNumberOfEventsToDisplay: number = 1;

  @ViewChild('dayRef') dayRef!: ElementRef<HTMLElement>;

  @HostListener('window:resize', ['$event']) onResizeHandler() {
    this.maxNumberOfEventsToDisplay = this.getMaxNumberOfEventsToDisplay();
  }

  ngOnInit(): void {
    this.calendarSrv.setMonth();
    this.eventSubscriptions = this.calendarSrv.getEvents().subscribe(events => {
      if(events){
        this.cdr.markForCheck();
      }
    });
  }

  ngAfterViewInit(): void {
    this.maxNumberOfEventsToDisplay = this.getMaxNumberOfEventsToDisplay();
  }

  getMaxNumberOfEventsToDisplay(){
    return this.calendarSrv.getMaxNumberOfEventsToDisplay(this.dayRef, this.singleEventHeight, this.bottomDayMargin);
  }

}
