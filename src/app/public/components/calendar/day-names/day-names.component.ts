import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-day-names',
  templateUrl: './day-names.component.html',
  styleUrls: ['./day-names.component.scss']
})
export class DayNamesComponent implements OnInit {

  dayNames = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
  dayShortNames = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
  innerWidth: number = 0;

  constructor() { }

  ngOnInit(): void {
    this.onResize();
  }

  @HostListener('window:resize')
  onResize = () => this.innerWidth = window.innerWidth;
}
