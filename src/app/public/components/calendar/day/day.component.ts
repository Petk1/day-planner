import { Component, Input, OnChanges, SimpleChanges, inject, ChangeDetectionStrategy } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';

import { EventDetailsComponent } from '../../modals/event-details/event-details.component';

import { IDay } from 'src/app/models/calendar-day.model';
import { IEvent } from 'src/app/models/event.model';


@Component({
  selector: 'app-day',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DayComponent implements OnChanges {
  @Input() day!: IDay;
  @Input() month!: number;
  @Input() year!: string;
  @Input() firstDayNumber: number = 0;
  @Input() maxNumberOfEventsToDisplay: number = 1;

  matDialog = inject(MatDialog);
  dialogConfig = new MatDialogConfig();
  modalDialog: MatDialogRef<EventDetailsComponent, any> | undefined;
  _showDots: boolean = false;

  ngOnChanges(changes: SimpleChanges): void {
    const changesDay = changes?.['day'];

    if(changesDay?.firstChange) return;

    const previousEvents: IEvent[] = changesDay?.previousValue?.['events'];
    const currentEvents: IEvent[] = changesDay?.currentValue?.['events'];

    if(previousEvents?.length !== currentEvents?.length || changes?.['maxNumberOfEventsToDisplay']){
      this.showDots();
    }
  }

  showDots(){
    const events = this.day.events;
    if(events && events?.length > this.maxNumberOfEventsToDisplay){
      this._showDots = true;
      return;
    }

    this._showDots = false;
  }

  showDetails() {
    if(!this.dayNumber) return;

    this.dialogConfig.id = 'event-details-modal-component';
    this.dialogConfig.data = {
      events: this.day.events,
      date: new Date(+this.year, this.month, this.dayNumber),
    }
    this.modalDialog = this.matDialog.open(EventDetailsComponent, this.dialogConfig);
  }

  get isSunday(){
    return this.day.dayNumber % 7 === 0;
  }

  get dayNumber(){
    const { isActive, dayNumber } = this.day ;
    return isActive ? dayNumber - (this.firstDayNumber - 1) : '';
  }

}
