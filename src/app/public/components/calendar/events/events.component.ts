import { Component, Input } from '@angular/core';
import { IEvent } from 'src/app/models/event.model';


@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent {
  @Input() events!: IEvent[];

}
