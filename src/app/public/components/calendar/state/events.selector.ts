import { createFeatureSelector, createSelector } from "@ngrx/store";
import { EventsState } from './events.state';
import { IDate } from './../../../../models/date.model';


export const EVENTS_STATE_NAME = 'events';

const getEventsState = createFeatureSelector<EventsState>(EVENTS_STATE_NAME);

export const getEvents = createSelector(getEventsState, state => {
  return state.events;
})

export const getEventsByDate = (date: IDate) => createSelector(getEventsState, state => {
  if(!state) return null;

  const year = <keyof EventsState>(date.year + '');
  const month = <keyof EventsState>(date.month + '');
  const day = <keyof EventsState>(date.day + '');
  const events = state?.events?.[year]?.[month]?.[day];

  return events ? events : null;
})
