import { createAction, props } from "@ngrx/store";

import { IUpdateEvent } from './../../../../models/update-event.model';
import { IDate } from './../../../../models/date.model';
import { INewEvent } from './../../../../models/new-event.model';
import { IEvents } from 'src/app/models/events.model';
import { IEventFromDb } from './../../../../models/event-from-db.model';
import { IActionPropsNewEvent } from './../../../../models/action-props-new-event.model';


const prefixName = '[events]';

export const LOAD_EVENTS_ACTION = `${prefixName} get events`;
export const LOAD_EVENTS_SUCCESS_ACTION = `${prefixName} get events success`;
export const ADD_EVENT_ACTION = `${prefixName} add event`;
export const ADD_EVENT_SUCCESS_ACTION = `${prefixName} add event success`;
export const UPDATE_EVENT_ACTION = `${prefixName} update event`;
export const UPDATE_EVENT_SUCCESS_ACTION = `${prefixName} update event success`;
export const DELETE_EVENT_ACTION = `${prefixName} delete event`;
export const DELETE_EVENT_SUCCESS_ACTION = `${prefixName} delete event success`;
export const DUMMY_ACTION = `${prefixName} dummy action`;

export const loadEvents = createAction(LOAD_EVENTS_ACTION);
export const loadEventsSuccess = createAction(LOAD_EVENTS_SUCCESS_ACTION, props<{ events: IEvents }>());
export const addEvent = createAction(ADD_EVENT_ACTION, props<{ event: INewEvent }>());
export const addEventSucces = createAction(ADD_EVENT_SUCCESS_ACTION, props<IActionPropsNewEvent>());
export const updateEvent = createAction(UPDATE_EVENT_ACTION, props<{ updateEvent: IUpdateEvent }>());
export const updateEventSucces = createAction(UPDATE_EVENT_SUCCESS_ACTION, props<IUpdateEvent>());
export const deleteEvent = createAction(DELETE_EVENT_ACTION, props<{ eventId: string, date: IDate }>());
export const deleteEventSucces = createAction(DELETE_EVENT_SUCCESS_ACTION, props<{ eventId: string, date: IDate }>());
export const dummyAction = createAction(DUMMY_ACTION);
