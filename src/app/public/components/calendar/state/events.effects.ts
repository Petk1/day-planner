import { Injectable, inject } from '@angular/core';

import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { of } from 'rxjs';
import { switchMap, mergeMap, map, withLatestFrom } from 'rxjs/operators';

import { AppState } from 'src/app/state/app.state';
import { getEvents } from './events.selector';
import { loadEvents, loadEventsSuccess, dummyAction, addEvent, addEventSucces, deleteEvent, deleteEventSucces, updateEvent, updateEventSucces } from './events.action';

import { EventsService } from 'src/app/public/services/events.service';


@Injectable()
export class EventsEffects {
  private actions$ = inject(Actions);
  private store = inject(Store<AppState>);
  private eventSrv = inject(EventsService);


  getEvents$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(loadEvents),
      withLatestFrom(this.store.select(getEvents)),
      switchMap(data => {
        const storedEvents = data[1];
        if(storedEvents){
          return of(dummyAction());
        }

        return this.eventSrv.getEvents().pipe(
          map(events => loadEventsSuccess({ events }))
        )
      })
    )
  })

  addEvent$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(addEvent),
      mergeMap(action => {
        return this.eventSrv.addEvent(action.event)
          .pipe(
            map(addedEvent => addEventSucces(addedEvent))
          )
      })
    )
  })

  updateEvent$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(updateEvent),
      mergeMap(action => {
        return this.eventSrv.updateEvent(action.updateEvent)
          .pipe(
            map(updatedEvent => updateEventSucces(updatedEvent))
          )
      })
    )
  })

  deleteEvent$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(deleteEvent),
      mergeMap(action => {
        return this.eventSrv.deleteEvent(action.eventId)
          .pipe(
            map(() => deleteEventSucces({ date: action.date, eventId: action.eventId }))
          )
      })
    )
  })

}
