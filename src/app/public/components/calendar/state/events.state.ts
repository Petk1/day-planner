import { IEvents } from 'src/app/models/events.model';


export interface EventsState {
  events: IEvents | null;
}

export const initialState: EventsState = {
  events: null,
}
