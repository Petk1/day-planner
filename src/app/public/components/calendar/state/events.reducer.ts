import { Action, createReducer, on } from "@ngrx/store";
import { EventsState, initialState } from "./events.state";
import { loadEventsSuccess, addEventSucces, deleteEventSucces, updateEventSucces } from './events.action';

import { cloneDeep } from 'lodash';

import { IDate } from 'src/app/models/date.model';
import { IEvent } from 'src/app/models/event.model';
import { sortEventsByTimestamp } from "src/app/shared/units/sort-events-by-timestamp";


const userEventsReducer = (state: EventsState, date: IDate, events: IEvent[]) => {
  const { day, month, year } = date;

  return {
    ...state.events,
    [year]: {
      ...state.events?.[year],
      [month]: {
        ...state.events?.[year]?.[month],
        [day]: [...events]
      },
    },
  }
}

const getUserEventsByDate = (state: EventsState, { day, month, year }: IDate) =>{
  let userEvents: IEvent[] = []

  if(state?.events?.[year]?.[month]?.[day]){
    userEvents = [...state.events[year][month][day]];
  }

  return cloneDeep(userEvents);
}

const _eventsReducer = createReducer(initialState,
  on(loadEventsSuccess, (state, action) => {
    return {
      ...state,
      events: action.events,
    }
  }),
  on(addEventSucces, (state, { addedEvent, date}) => {
    const userEvents = getUserEventsByDate(state, date);
    userEvents.push(addedEvent);
    sortEventsByTimestamp(userEvents);
    return {
      ...state,
      events: userEventsReducer(state, date, userEvents),
    }
  }),
  on(updateEventSucces, (state, { id, name, content, done, date, dateObject }) => {
    if(!dateObject){
      return { ...state };
    }
    const userEvents = getUserEventsByDate(state, dateObject);
    const eventIndex =  userEvents.findIndex(userEvent => userEvent.id === id);
    const timestamp = date?.getTime();

    userEvents[eventIndex].name = name;
    userEvents[eventIndex].content = content;
    userEvents[eventIndex].done = done;
    if(timestamp) userEvents[eventIndex].timestamp = timestamp;
    sortEventsByTimestamp(userEvents);

    return {
      ...state,
      events: userEventsReducer(state, dateObject, userEvents),
    }
  }),
  on(deleteEventSucces, (state, { eventId, date }) => {
    let userEvents = getUserEventsByDate(state, date);
    userEvents = userEvents.filter(singleEvent => singleEvent.id !== eventId);

    return {
      ...state,
      events: userEventsReducer(state, date, userEvents),
    }
  })
);

export function eventsReducer(state: EventsState | undefined, action: Action) {
  return _eventsReducer(state, action);
}
