import { Component } from '@angular/core';

enum Position {
  LEFT = 'left',
  CENTER = 'center',
  RIGHT = 'right'
}

interface Option {
  optionId: number,
  name: string;
  currentPosition: Position,
  previousPosition: Position,
}

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent {

  position = Position;
  options: Option[] = [
    {
      optionId: 0,
      name: 'Calendar',
      currentPosition: Position.LEFT,
      previousPosition: Position.LEFT
    },
    {
      optionId: 1,
      name: 'Option II',
      currentPosition: Position.CENTER,
      previousPosition: Position.CENTER
    },
    {
      optionId: 2,
      name: 'Option III',
      currentPosition: Position.RIGHT,
      previousPosition: Position.RIGHT
    }
  ]

  moveOptions(optionId: number): void {
    const positionOfTheClickedOption = this.options[optionId].currentPosition;

    if (positionOfTheClickedOption === Position.CENTER) return;

    const indexCenterOption = this.options.findIndex(option => option.currentPosition === Position.CENTER);

    // swap position of these options
    this.options[indexCenterOption].currentPosition = positionOfTheClickedOption;
    this.options[optionId].previousPosition = positionOfTheClickedOption;
    this.options[optionId].currentPosition = Position.CENTER;
  }

}
