import { Injectable, inject } from "@angular/core";

import { Store } from "@ngrx/store";
import { Actions, ofType } from "@ngrx/effects";
import { AppState } from "src/app/state/app.state";
import { getEventsByDate } from "../components/calendar/state/events.selector";
import { DELETE_EVENT_SUCCESS_ACTION, UPDATE_EVENT_SUCCESS_ACTION, deleteEvent, updateEvent } from "../components/calendar/state/events.action";

import { switchMap, take } from "rxjs";

import { IDate } from "src/app/models/date.model";
import { IEvent } from "src/app/models/event.model";
import { IDisplayedEvent } from "src/app/models/displayed-event.model";
import { IUpdateEvent } from "src/app/models/update-event.model";


@Injectable()
export class EventDetailsService {
  store = inject(Store<AppState>);
  actions$ = inject(Actions);
  objectDate!: IDate;
  events: IDisplayedEvent[] = [];

  setDate(date: Date){
    this.objectDate = {
      day: +date.toLocaleString('en', { day: 'numeric' }),
      month: +date.toLocaleString('en', { month: 'numeric' }),
      year: +date.toLocaleString('en', { year: 'numeric' }),
    }
  }

  setEvents(events: IEvent[], setAllNotSelected: boolean, selectedEventIndex?: number | null) {
    this.events = events.map((singleEvent, index) => ({
      ...singleEvent,
      selected: setAllNotSelected ? false : selectedEventIndex === index })
    );
  }

  doneNotDoneEventSwitch({ id, name, content, done }: IDisplayedEvent, date: Date){
    const eventToUpdate: IUpdateEvent = { id, name, content, date, done: !done };
    this.store.dispatch(updateEvent({ updateEvent: eventToUpdate }));
    return this.listenOnSuccessAction(UPDATE_EVENT_SUCCESS_ACTION);
  }

  deleteEvent(eventId: string){
    this.store.dispatch(deleteEvent({ eventId, date: this.objectDate }));
    return this.listenOnSuccessAction(DELETE_EVENT_SUCCESS_ACTION)
  }

  listenOnSuccessAction(actionName: string) {
    return this.actions$.pipe(
      ofType(actionName),
      take(1),
      switchMap(() => this.getEventsByDate())
    )
  }

  getEventsByDate(){
    return this.store.select(getEventsByDate(this.objectDate));
  }

}
