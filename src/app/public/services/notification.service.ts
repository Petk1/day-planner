import { Injectable, inject } from '@angular/core';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';

import { NotificationComponent } from 'src/app/shared/modals/notification/notification.component';
import { INotificationConfig, INotificationInitData } from './../../models/notification-config.model';


@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private readonly matDialog = inject(MatDialog);
  private modalDialog: MatDialogRef<NotificationComponent, any>[] = [];
  private dialogConfig = new MatDialogConfig();


  createAndOpenNewNotification(config: INotificationConfig) {
    this.dialogConfig.width = config?.width || '440px';
    this.dialogConfig.height = config?.height || '275px';
    this.dialogConfig.disableClose = config?.mode === 'waiting';
    const { title, message, mode }: INotificationInitData = config;
    this.dialogConfig.data = { title, message, mode };

    if(config?.closeAllModalFirst){
      this.closeAllNotifications();
    }

    const newModalDialog = this.matDialog.open(NotificationComponent, this.dialogConfig);
    this.modalDialog?.push(newModalDialog);
    return newModalDialog;
  }

  closeAllNotifications() {
    if(!this.modalDialog || !Array.isArray(this.modalDialog)) return;

    this.modalDialog.forEach(modal => modal.close());
  }
}
