import { Injectable, inject } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { Store } from "@ngrx/store";
import { Actions, ofType } from '@ngrx/effects';
import { AppState } from "src/app/state/app.state";
import { addEvent, updateEvent } from "../components/calendar/state/events.action";

import { take } from 'rxjs';

import { NotificationService } from "./notification.service";

import { HandleDate } from "src/app/shared/units/handle-date";
import { INewEvent } from "src/app/models/new-event.model";
import { IUpdateEvent } from "src/app/models/update-event.model";
import { IDisplayedEvent } from "src/app/models/displayed-event.model";


Injectable()
export class HandleEventService {
  private readonly store = inject(Store<AppState>)
  private readonly actions$ = inject(Actions);
  private readonly fb = inject(FormBuilder);
  private readonly notificationSrv = inject(NotificationService);

  private currentEventName: string = '';
  private currentEventContent: string = '';
  private currentEventTime: string = '';
  private _date!: Date;
  private _currentEventId!: string;
  private _currentEventStatus!: boolean;

  public handleEventForm!: FormGroup;


  setForm(currentEventData: IDisplayedEvent | undefined){
    if(currentEventData){
      const { name, content, timestamp } = currentEventData;
      this.currentEventName = name;
      this.currentEventContent = content;
      this.currentEventTime = new Date(timestamp).toTimeString().split(' ')[0].substring(0, 5);
    }

    this.handleEventForm = this.fb.group({
      eventName: [this.currentEventName, [Validators.required, Validators.minLength(1), Validators.maxLength(40)]],
      eventContent: [this.currentEventContent, Validators.maxLength(250)],
      eventTime: [this.currentEventTime, Validators.required],
    });
  }

  updateEvent(editOnlyTime: boolean){
    if(!this.handleEventForm.dirty) return;

    const updateEventData = this.updateEventData;

    if(!this.isDiffEventData(updateEventData)) {
      this.notificationSrv.createAndOpenNewNotification({
        title: 'No changes',
        message: 'You need to change some value before updating the event',
        mode: 'information',
        closeAllModalFirst: true,
      })

      return;
    }

    if(editOnlyTime){
      this.store.dispatch(updateEvent({ updateEvent: updateEventData }));
      return;
    }

    this.store.dispatch(updateEvent({ updateEvent: updateEventData }));
  }

  addEvent(){
    this.store.dispatch(addEvent({ event: this.newEventData }));
  }

  isDiffEventData({ name, content, date }: IUpdateEvent){
    if(name !== this.currentEventName) return true;
    if(content !== this.currentEventContent) return true;
    if(date?.toTimeString().split(' ')[0].substring(0, 5) !== this.currentEventTime) return true;

    return false;
  }

  isActionSuccess(actionName: string){
    return this.actions$.pipe(
      ofType(actionName),
      take(1),
    )
  }

  set date(value: Date){
    this._date = value;
  }

  get date(){
    return this._date;
  }

  set currentEventId(value: string){
    this._currentEventId = value;
  }

  get currentEventId(){
    return this._currentEventId;
  }

  set currentEventStatus(value: boolean){
    this._currentEventStatus = value;
  }

  get currentEventStatus(){
    return this._currentEventStatus;
  }

  get updateEventData(): IUpdateEvent {
    return {
      id: this.currentEventId,
      name: this.eventName?.value,
      content: this.eventContent?.value,
      done: this.currentEventStatus,
      date: this.dateEventWithTime
    }
  }

  get newEventData(): INewEvent {
    return {
      name: this.eventName?.value,
      content: this.eventContent?.value,
      done: false,
      date: this.dateEventWithTime,
    }
  }

  get dateEventWithTime(){
    const date = new Date(`${HandleDate.getYearMonthDay(this.date)}T${this.eventTime?.value}Z`);
    return HandleDate.getDateMinusTimezonOffset(date);
  }

  get eventName() {
    return this.handleEventForm.get('eventName');
  }

  get eventContent() {
    return this.handleEventForm.get('eventContent');
  }

  get eventTime(){
    return this.handleEventForm.get('eventTime');
  }

  get eventNameLegnth() {
    return this.eventName?.value?.length;
  }

  get eventContentLegnth() {
    return this.eventContent?.value?.length;
  }

}
