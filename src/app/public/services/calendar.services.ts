import { Injectable, inject, ElementRef } from "@angular/core";

import { Store } from "@ngrx/store";

import { tap } from 'rxjs/operators';

import * as moment from 'moment';

import { AppState } from "src/app/state/app.state";
import { loadEvents } from "../components/calendar/state/events.action";
import { getEvents } from "../components/calendar/state/events.selector";

import { IEvent } from "src/app/models/event.model";
import { IEvents } from "src/app/models/events.model";
import { IDay } from "src/app/models/calendar-day.model";
import { IMonth } from "src/app/models/calendar-month.model";


Injectable()
export class CalendarService {
  store = inject(Store<AppState>);
  date = moment();
  events!: IEvents;
  monthName: string = '';
  monthNumber: number = 1;
  year: string = '';
  monthDisplayed!: IMonth;
  displayedNumberDays: number = 35;
  readonly numberDaysWeek: number = 7;
  numberOfWeeks: number = 5;

  getEvents() {
    this.store.dispatch(loadEvents());
    return this.store.select(getEvents)
      .pipe(
        tap(events => {
          if(events){
            this.events = events;
            this.setMonth();
          }
        })
      )
  }

  setMonth() {
    this.setMonthNameAndYear();
    const firstDayNumber = moment(this.date).startOf('M').isoWeekday(); // number of the first day of the month
    const numberDays = this.date.daysInMonth();
    const days = this.setDays(firstDayNumber, numberDays);
    this.monthDisplayed = { firstDayNumber, numberDays, days };
  }

  setDays(firstDayNumber: number, numberDays: number): IDay[] {
    const days: IDay[] = [];
    this.displayedNumberDays = (firstDayNumber - 1) + numberDays > 35 ? 42 : 35;
    this.numberOfWeeks = this.displayedNumberDays / this.numberDaysWeek;

    for (let dayNumber = 1; dayNumber <= this.displayedNumberDays; dayNumber++) {
      const dayNumberCondition = this.dayNumberCondition(dayNumber, firstDayNumber, numberDays);
      days.push({
        dayNumber,
        isActive: dayNumberCondition,
        events: dayNumberCondition ? this.getEventsOfDay(dayNumber, firstDayNumber) : []
      })
    }

    return days;
  }

  setMonthNameAndYear() {
    this.monthName = this.date.format('MMMM');
    this.monthNumber = this.date.month();
    this.year = this.date.format('YYYY');
  }

  getEventsOfDay(dayNumber: number, firstDayNumber: number): IEvent[] {
    const year = this.date.year();
    const month = this.date.month() + 1;
    const day = dayNumber - firstDayNumber + 1;

    return this.events?.[year]?.[month]?.[day];
  }

  nextMonth() {
    this.date.add(1, 'M');
    this.setMonth();
  }

  previousMonth() {
    this.date.subtract(1, 'M');
    this.setMonth();
  }

  getMaxNumberOfEventsToDisplay(dayRef: ElementRef<HTMLElement>, singleEventHeight: number, bottomDayMargin: number){
    const dayHeight = dayRef.nativeElement.offsetHeight - bottomDayMargin;
    return Math.floor(dayHeight / singleEventHeight);
  }

  dayNumberCondition(dayNumber: number, firstDayNumber: number, numberDays: number): boolean {
    return dayNumber >= firstDayNumber && dayNumber < numberDays + firstDayNumber;
  }

}
