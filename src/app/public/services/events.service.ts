import { Injectable, inject } from "@angular/core";
import { AngularFirestore } from "@angular/fire/compat/firestore";

import { from } from "rxjs";
import { map, switchMap } from 'rxjs/operators';

import { AuthService } from "src/app/core/services/auth.service";

import { IActionPropsNewEvent } from 'src/app/models/action-props-new-event.model';
import { IEventFromDb } from 'src/app/models/event-from-db.model';
import { IUpdateEvent } from 'src/app//models/update-event.model';
import { INewEvent } from 'src/app/models/new-event.model';
import { IEvents } from 'src/app/models/events.model';
import { IEvent } from 'src/app/models/event.model';
import { IDate } from 'src/app/models/date.model';

import { WaitingForResult } from "src/app/shared/decorators/waiting-for-result";
import { FirestoreErrorHandler } from "src/app/shared/decorators/firestore-error-handler";
import { sortEventsByTimestamp } from "src/app/shared/units/sort-events-by-timestamp";


@Injectable()
export class EventsService {
  dbFirestore = inject(AngularFirestore);
  authSrv = inject(AuthService);


  @FirestoreErrorHandler
  public getEvents() {
    return this.authSrv.getUserInfo()
      .pipe(
        this.authSrv.checkUid(),
        switchMap((uid: string) => {
          return this.dbFirestore.collection(`Users/${uid}/Events`).get().pipe(
            map(response => {
              if(!response) return { };

              const eventsFromDb = response.docs.map(event => {
                const data = event.data() as IEventFromDb;
                data.id = event.id;
                return data;
              })

              return this.getEventsDateToStore(eventsFromDb);
            })
          )
        })
      )
  }

  private getEventsDateToStore(eventsFromDb: IEventFromDb[]): IEvents {
    let eventsToStore: IEvents = {};

    eventsFromDb.forEach(singleEventFromDb => {
      const timestamp = singleEventFromDb.date.seconds * 1000;
      const dateInfo = this.getDateInfoByTimestamp(timestamp);
      const singleEvent = this.getSingleEventDate(singleEventFromDb, timestamp);
      eventsToStore = { ...this.addEventToStoreObject(eventsToStore, dateInfo, singleEvent) };
    })

    return eventsToStore;
  }

  private addEventToStoreObject(eventsToStore: IEvents, date: IDate, singleEvent: IEvent) {
    const { day, month, year } = date;

    let events: IEvent[] = [singleEvent];
    if(eventsToStore?.[year]?.[month]?.[day]){
      events = [...events, ...eventsToStore?.[year]?.[month]?.[day]];
      sortEventsByTimestamp(events);
    }

    return {
      ...eventsToStore,
      [year]: {
        ...eventsToStore?.[year],
        [month]: {
          ...eventsToStore?.[year]?.[month],
          [day]: [
            ...events,
          ],
        },
      },
    }
  }

  @FirestoreErrorHandler
  @WaitingForResult()
  public addEvent(newEvent: INewEvent) {
    return this.authSrv.getUserInfo()
      .pipe(
        this.authSrv.checkUid(),
        switchMap((uid: string) => {
          return from(this.dbFirestore.collection(`Users/${uid}/Events`)
            .add(newEvent)
          ).pipe(
            map(response => {
              const timestamp = newEvent.date.getTime();
              const addedEvent: IEvent = {
                id: response.id,
                name: newEvent.name,
                done: newEvent.done,
                content: newEvent.content,
                timestamp,
              }
              const date = this.getDateInfoByTimestamp(timestamp);
              return <IActionPropsNewEvent>{
                addedEvent: addedEvent,
                date,
              };
            })
          )
        })
      )
  }

  @FirestoreErrorHandler
  @WaitingForResult()
  public updateEvent({ id, name, content, done, date }: IUpdateEvent){
    return this.authSrv.getUserInfo()
      .pipe(
        this.authSrv.checkUid(),
        switchMap((uid: string) => {
          return from(this.dbFirestore.collection(`Users/${uid}/Events`)
            .doc(id)
            .update({
              name,
              content,
              done,
              date,
            })
          ).pipe(
            map(() => {
              return <IUpdateEvent>{
                id,
                name,
                content,
                done,
                date,
                dateObject: date && this.getDateInfoByTimestamp(date.getTime())
              }
            })
          )
        })
      )
  }

  @FirestoreErrorHandler
  @WaitingForResult()
  public deleteEvent(eventId: string){
    return this.authSrv.getUserInfo()
      .pipe(
        this.authSrv.checkUid(),
        switchMap((uid: string) => {
          return this.dbFirestore.collection(`Users/${uid}/Events`).doc(eventId).delete()
        })
      )
  }

  private getSingleEventDate({ id, name, content, done }: IEventFromDb, timestamp: number): IEvent {
    return {
      id,
      name,
      content,
      done,
      timestamp,
    }
  }

  private getDateInfoByTimestamp(timestamp: number): IDate {
    const newDate = new Date(timestamp);
    return {
      day: newDate.getDate(),
      month: newDate.getMonth() + 1,
      year: newDate.getFullYear()
    }
  }
}
