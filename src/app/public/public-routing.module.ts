import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AngularFireAuthGuard, redirectUnauthorizedTo } from '@angular/fire/compat/auth-guard';

import { LoginComponent } from './../features/auth/login/login.component';
import { RegistrationComponent } from './../features/auth/registration/registration.component';
import { MainPageComponent } from './components/main-page/main-page.component';


const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['']);

const routes: Routes = [
  {
    path: '',
    component: MainPageComponent,
    canActivate: [ AngularFireAuthGuard ],
    data: { authGuardPipe: redirectUnauthorizedToLogin }
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class PublicRoutingModule { }
