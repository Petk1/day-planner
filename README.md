
# Day Planner

The Angular project built with Firebase to create a website that allows users to schedule their days. Create your own account and organize your calendar.


**Live Demo:** [Day Planner Demo](https://day-planner-indol.vercel.app)

## Getting started

Make sure you have the Angular CLI installed.  

To manage dependencies you can use yarn or npm.  
Depending on what you choose, run `yarn install` or `npm install`.

Once all dependencies are resolved run `ng serve` for a dev server.  
Then open your browser and navigate tn `http://localhost:4200/`.

## Functionality overview

* Create account (login and sign up pages)
* CRUD events
* Drag and Drop events
* Overview of all events in calendar view
* Forgot Password: Reset the password by using the link sent via email

<div>
  <p align="center">
    <img src="src/assets/logos/angular_logo.png" alt="angular-logo" width="110px" height="122px"/>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <img src="src/assets/logos/ngrx_logo.png" alt="ngrx-logo" width="120px" height="120px"/>
  </p>
  <p align="center">
    <img src="src/assets/logos/firebase-logo-built_black.png" alt="ngrx-logo" width="269px" height="120px"/>
  </p>
</div>
